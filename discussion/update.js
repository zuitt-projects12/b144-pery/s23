db.users.updateOne(
{firstName: "Test"} /*this line is to set the criteria to find the certain document to be updated*/, 
{$set: /* this line is the set of data to be updated */{
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "1234",
        email: "bill@something.com"
        },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations"
    }
})