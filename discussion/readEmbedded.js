/* reading documewnts thru embedded */

/*db.users.find({
    contact: {
        phone: "09171234567",
        email:"jasmin@something.com"
        }
    })*/
    
/* reading documents thru dot notation */
/*db.users.find({
    "contact.email": "jasmin@something.com"
    })*/
    
/*reading arrays*/
db.users.find({
    courses: ["CSS", "Javascript", "Python"]
    })

/*reading arrays without orders*/
db.users.find({
    courses: {$all: ["React", "Python"]}
    })